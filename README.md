Collate catalogs


Examples
--------


```bash

#
# collating metacal at SLAC
#

# directory holding all metacal outputs
metacal_dir='/nfs/slac/g/ki/ki19/des/esheldon/ngmixer-output/y1a1-main-001/output'

output_file='DES-y1a1-main-001-blind-v5.fits'

desy1-mcal-collate \
    $metacal_dir \
    $output_file
# repeated for y1a1-s82-001 and y1a1-D04-011

# combine the regions
desy1-mcal-combine-regions

```

```bash
#
# collating gold+mof
# sql scripts are in the /sql sub-directory
#

# left join of gold with coadd and mof main

sql="~/git/desy1_matchedcat/sql/query-mof-main-gold.sql"
main_gold="gold-mof-main-left-nobadregions.fits"
des-query \
    --replace-none=-9999 \
    -f fits \
    -o "$main_gold" < "$sql" 

# inner join of mof with the coadd; we will merge these
sql="~/git/desy1_matchedcat/sql/query-mof-d04-gold.sql"
d04_gold="gold-mof-d04-inner-nobadregions.fits"
des-query \
    -f fits \
    -o "$d04_gold" < "$sql"

# combine the above results, calculating additional
# columns as needed

gold_file="y1a1-gold-mof-badregion.fits"
badmask="y1a1_gold_1.0.2gc_wide_badmask_4096.fit"
desy1-merge-gold-badmask \
    "$main_gold" \
    "$d04_gold" \
    "$badmask" \
    "$gold_file"

# test the collation of 1000 random objects
# against the original tables
desy1-test-gold "$gold_file" 1000
desy1-test-gold "$gold_file" 1000 --d04
```


```bash
# matching metacal and gold SLAC

desy1-mcal-reorder \
    y1a1-gold-mof-badregion.fits \
    mcal-y1a1-combined-riz-blind-v1.fits \
    mcal-y1a1-combined-riz-blind-v2-matched.fits

desy1-mcal-reorder \
    y1a1-gold-mof-badregion.fits \
    mcal-y1a1-combined-griz-blind-v1.fits \
    mcal-y1a1-combined-griz-blind-v2-matched.fits
```

```bash
# test the collation against 
# 3 random tiles from the metacal matched catalog

# directory holding all metacal outputs
metacal_dir='/nfs/slac/g/ki/ki19/des/esheldon/ngmixer-output/y1a1-main-001/output'

# collated, matched shear file
shear_file='mcal-y1a1-combined-riz-blind-v2-matched.fits'

desy1-mcal-test $metacal_dir $shear_file

# test more tiles
desy1-mcal-test $metacal_dir $shear_file --ntest=10

# repeat the same tiles by setting the seed
desy1-mcal-test $metacal_dir $shear_file --seed=314159

```
