from __future__ import print_function
import os
import numpy as np
import fitsio
import glob
import esutil as eu
import time

from . import im3_util


class Collator(object):
    """
    collate the tiles, renaming fields to a common standard.
    """
    def __init__(self, metacal_dir, output_file):
        self._metacal_dir=metacal_dir
        self._output_file=output_file
        self._load_flist()

    def go(self):
        """
        go by tile and write the data
        """
        nfiles=len(self._flist)
        print("writing to:",self._output_file)

        first=True

        with fitsio.FITS(self._output_file, 'rw', clobber=True) as fits:
            for i, fname in enumerate(self._flist):

                print("%d/%d %s" % (i + 1, nfiles, fname))

                print("    reading")
                data = fitsio.read(fname)

                print("    extracting output")

                # odata = self._extract_output(data)
                odata = data
                if first:
                    first = False
                    fits.write(odata)
                else:
                    fits[-1].append(odata)

                    # if i==4:
                    #    return

    def _extract_output(self, data):
        """
        extract the outputs, following the name mapping.

        Calculate any extra fields
        """


    def _load_flist(self):
        """
        get the list of files
        """
        self._flist = im3_util.load_mcal_flist(self._metacal_dir)