from __future__ import print_function
from collections import OrderedDict
import os
import glob
import copy

DEFAULT = -9999
PDEFAULT = 9999

IM3_FILE_PATTERN='DES*.fits'


def load_mcal_flist(dir):
    pattern = os.path.join(dir, IM3_FILE_PATTERN)

    flist = glob.glob(pattern)
    flist.sort()

    if len(flist) == 0:
        raise RuntimeError("found no files '%s'" % pattern)

    return flist