"""
re-order the catalog to match line-by-line with the referece
gold catalog
"""
from __future__ import print_function
import numpy
from numpy import arange, zeros, where
import fitsio
import esutil as eu
import esutil.numpy_util as nu
import time

from . import mcal_util


class Orderer(object):
    """
    take the collated file and re-order it to match the order
    of the gold file

    For objects that do not match, gold data will be copied in
    but shear data will not

    Need to set new flags
    """
    def __init__(self,
                 gold_file,
                 shear_file,
                 output_file):

        self._gold_file=gold_file
        self._shear_file=shear_file
        self._output_file=output_file


        print("writing to:",self._output_file)

        assert self._gold_file != self._output_file
        assert self._shear_file != self._output_file

        #self._load_gold_ids()
        self._load_shear_ids()

        self._set_combined_dtype()
    
    def go(self, test_nchunks=None):
        """
        write new shear and gold files that line up 1-1
        """

        chunksize = mcal_util.CHUNKSIZE
        first=True

        with fitsio.FITS(self._output_file,'rw',clobber=True) as fits:

            with fitsio.FITS(self._gold_file) as gfits:

                ngold = gfits[1].get_nrows()
                nchunks = self._get_chunk_info(ngold, test_nchunks=test_nchunks)

                for chunk in xrange(nchunks):

                    print("chunk %d/%d" % (chunk+1,nchunks))

                    start = chunk*chunksize
                    end = (chunk+1)*chunksize

                    gdata = self._read_from_gold(gfits, start, end)

                    odata = self._get_matched_data(gdata)

                    print("    writing ",end='')
                    tm0=time.time()
                    if first:
                        first=False
                        fits.write(odata,extname=mcal_util.EXTNAME)
                    else:
                        fits[-1].append(odata)
                    tm=time.time()-tm0
                    print("%g sec" % tm)

        print("output is in:",self._output_file)

    def _read_from_gold(self, fits, start, end):
        print("    reading from gold",end='')

        tm0=time.time()
        data = fits[1][start:end]

        print(" %g sec" % (time.time()-tm0))

        return data

    def _get_chunk_info(self, ngold, test_nchunks=None):
        chunksize = mcal_util.CHUNKSIZE

        nchunks = ngold//chunksize
        if (ngold % chunksize) != 0:
            nchunks += 1

        if test_nchunks is not None and test_nchunks < nchunks:
            nchunks=test_nchunks

        return nchunks

    def _match_gold(self, gids, sids):
        print("    matching",end='')
        tm0=time.time()
        mg, ms = eu.numpy_util.match(gids, sids)
        tm=time.time()-tm0
        nmatch=mg.size
        print(" %d/%d matched %g sec" % (mg.size,gids.size,tm))

        return mg, ms

    def _read_shear_subset(self, index):
        print("    reading the shear subset",end='')
        tm0=time.time()
        data = fitsio.read(self._shear_file, rows=index)
        tm=time.time()-tm0
        print(" %g sec" % tm)

        return data

    def _copy_from_shear(self, data, sdata, mo, ms):
        print("    copying shear data",end='')
        tm0 = time.time()
        for n in sdata.dtype.names:
            data[n][mo] = sdata[n][ms]
        print(" %g sec" % (time.time()-tm0))

    def _get_matched_data(self, gdata):
        """
        get versions of the data that line up row-by-row
        """

        odata = self._make_combined_struct(gdata.size)

        nu.copy_fields(gdata, odata)

        mo, ms = self._match_gold(
            odata['coadd_objects_id'],
            self._shear_ids,
        )
        nmatch = mo.size
        if nmatch == 0:
            # Note flag columns are by default already set to FLAG_NOT_MEASURED
            # so returning early leaves that in place
            return odata
        
        sdata = self._read_shear_subset(ms)

        # fitsio will return the data in row-ascending order, so we need to
        # re-match

        mo, ms = self._match_gold(
            odata['coadd_objects_id'],
            sdata['coadd_objects_id'],
        )

        if mo.size != nmatch:
            raise ValueError("number of matches changed: "
                             "orig: %d new: %d" % (nmatch,mo.size))

        self._copy_from_shear(odata, sdata, mo, ms)

        self._set_select_flags(gdata, odata, mo)

        return odata


    def _set_select_flags(self, gdata, data, index):
        """
        Note flag columns are by default already set to FLAG_NOT_MEASURED 

        gdata and data line up 1-1

        index is things for which we had an entry
        in the shear catalog

        parameters
        ----------
        gdata: array
            Gold data
        data: array
            Output array
        index: array
            Index into output array
        """

        print("    setting select flags")
        tm0 = time.time()

        fdict = mcal_util.MCAL_SELECT_FLAGS 

        # for things with a match, we start assuming we have something
        data['flags_select'][index] = 0


        #
        # cuts common to sheared and unsheared measurements
        #

        w,=where(gdata['mof_flags'][index] != 0)
        if w.size > 0:
            print("        %6d/%6d with mof_flags " % (w.size, index.size))
            data['flags_select'][index[w]] |= fdict['mof_flags']

        # BPZ cannot handle negative fluxes.  We hope this cut doesn't bias
        # us too much
        self._check_mof_fluxes(gdata, data, index)

        # dec cut
        w,=where(gdata['dec'][index] < mcal_util.DEC_MIN)
        if w.size > 0:
            print("        %6d/%6d with dec "
                  "less than %g" % (w.size, index.size,mcal_util.DEC_MIN))
            data['flags_select'][index[w]] |= fdict['dec_cut']



        # bad regions
        w,=where(gdata['flags_badregion'][index] != 0)
        if w.size > 0:
            print("        %6d/%6d with badregion set" % (w.size, index.size))
            data['flags_select'][index[w]] |= fdict['flags_badregion']



        # gold flags
        w,=where(gdata['flags_gold'][index] != 0)
        if w.size > 0:
            print("        %6d/%6d with gold flags set" % (w.size, index.size))
            data['flags_select'][index[w]] |= fdict['flags_gold']



        # metacal measurement flags
        w,=where(data['flags'][index] != 0)
        if w.size > 0:
            print("        %6d/%6d with flags set" % (w.size, index.size))
            data['flags_select'][index[w]] |= fdict['mcal_flags']



        #
        # Now cuts that may be different for sheared versions
        #

        # start all the sheared flags with copy of the unsheared flags
        current_flags = data['flags_select']
        for type in ['1p','1m','2p','2m']:
            n=mcal_util.get_namer(type=type)
            fname = n('flags_select')
            
            data[fname] = current_flags



        for type in ['noshear','1p','1m','2p','2m']:
            n=mcal_util.get_namer(type=type)
            fname = n('flags_select')

            # size cut
            w,=where(
                (data['flags'][index] == 0)
                &
                (data[n('size')][index] < mcal_util.TRATIO_CUT*data['mcal_psf_size'][index])
            )
            if w.size > 0:
                print("        %6d/%6d with flags==0 and low Tratio %s" % (w.size, index.size,type))
                data[fname][index[w]] |= fdict['size_cut']



            # snr cut
            w,=where(
                (data['flags'][index] == 0)
                &
                (data[n('snr')][index] < mcal_util.SNR_CUT)
            )
            if w.size > 0:
                print("        %6d/%6d with flags==0 and low snr %s" % (w.size, index.size,type))
                data[fname][index[w]] |= fdict['snr_cut']


            # flux cuts
            self._check_mcal_flux(n, data, index)


        print("     %g sec" % (time.time()-tm0))

    def _check_mof_fluxes(self, gdata, data, index):
        """
        parameters
        ----------
        gdata: array
            Gold data
        data: array
            Output data
        index: array
            Indexes for rows we are working with
        """

        fdict = mcal_util.MCAL_SELECT_FLAGS 

        for band in mcal_util.MOF_BANDS:

            field='mof_flux_%s' % band
            w,=numpy.where(gdata[field][index] < mcal_util.MINFLUX)

            if w.size > 0:

                m="        %6d/%6d with low %s < %g"
                print(m % (w.size, index.size, field, mcal_util.MINFLUX))

                data['flags_select'][index[w]] |= fdict['mof_lowflux']

    def _check_mcal_flux(self, n, data, index):
        """
        Note we only set this for things with mcal flags==0

        parameters
        ----------
        n: Namer
            Namer for one of the sheared types
        data: array
            Output data
        index: array
            Subset to work with
        """

        fdict = mcal_util.MCAL_SELECT_FLAGS 

        for band in mcal_util.MOF_BANDS:
            field = n('flux_%s' % band)

            if field in data.dtype.names:

                w,=numpy.where(
                    (data['flags'][index] == 0)
                    &
                    (data[field][index] < mcal_util.MINFLUX)
                )

                if w.size > 0:
                    m="        %6d/%6d with low %s < %g"
                    print(m % (w.size, index.size, field, mcal_util.MINFLUX))

                    data[n('flags_select')][index[w]] |= fdict['mcal_lowflux']


    ''''
    def _combine_data(self, data, gdata):
        """
        combine the shear and gold data
        """
        new_data = self._make_combined_struct(data.size)

        # we want whatever is in the  the shear structure
        # to override if there are name conflicts

        nu.copy_fields(gdata, new_data)
        nu.copy_fields(data, new_data)
        
        return new_data
    '''

    '''
    def _load_gold_ids(self):
        """
        load the gold ids for matching
        """
        print("reading ids from:",self._gold_file)
        self._gold_ids = fitsio.read(
            self._gold_file,
            columns='coadd_objects_id',
            lower=True,
        )
    '''
    def _load_shear_ids(self):
        """
        load the shear ids for matching
        """
        print("reading ids from:",self._shear_file)
        self._shear_ids = fitsio.read(
            self._shear_file,
            columns='coadd_objects_id',
        )


    def _set_combined_dtype(self):
        """
        get a combined dtype, avoiding duplicates
        """
        tmp = fitsio.read(self._shear_file, rows=[0])

        dt = tmp.dtype.descr
        names = tmp.dtype.names

        gold_tmp = fitsio.read(self._gold_file, rows=[0])
        for d in gold_tmp.dtype.descr:
            n = d[0]
            if n in mcal_util.MOF_COLS_TO_COPY and n not in names:
                print("copying",n,"from gold")
                dt.append( d )

        dt += [
            ('flags_select','i4'),
            ('flags_select_1p','i4'),
            ('flags_select_1m','i4'),
            ('flags_select_2p','i4'),
            ('flags_select_2m','i4'),
        ]
        self._dtype = dt


    def _make_combined_struct(self, n):
        """
        get the combined structure
        """
        st = zeros(n, dtype=self._dtype)
        mcal_util.set_defaults(st)

        return st


