import numpy as np
import fitsio as fio
import healpy as hp

goldname = '/global/cscratch1/sd/tvarga/WLCAT/release/Y1A1_GOLD_1_0_3_match_3.fits'
badname = '/global/cscratch1/sd/tvarga/metacal2/y1a1_gold_1.0.2gc_wide_badmask_4096.fit.gz'

ogoldname = '/global/cscratch1/sd/tvarga/WLCAT/release/Y1A1_GOLD_1_0_3_wide_badmask_match_3.fits'


gold = fio.FITS(goldname)[1]

bpix = hp.read_map(badname)


ra = gold['ra'][:]
dec = gold['dec'][:]

theta = (90. - dec) * np.pi / 180.
phi = ra * np.pi / 180.

nside = hp.get_nside(bpix)

pixes = hp.ang2pix(nside, theta, phi)

bflags = bpix[pixes]

gdata = gold[:]
gdata['flags_badregion'] = bflags

fio.write(ogoldname, gdata, clobber=True)



