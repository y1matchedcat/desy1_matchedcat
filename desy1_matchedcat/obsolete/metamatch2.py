"""
collate the metacal files.

Note we run a second pass to reorder according to the gold catalog,
so we don't actually need to do the matching here at all. It would
speed things up quite a bit to do so
"""
# note I set the defaults for integers to -9999 rather than -1

from __future__ import print_function
import os
import numpy as np
import fitsio
import glob
import esutil as eu
import time
from collections import OrderedDict

import argparse

parser=argparse.ArgumentParser()

parser.add_argument('gold_file',help='gold catalog with tilename')
parser.add_argument('metacal_dir',help='directory holding all metacal files')
parser.add_argument('output_file',help='output file to write')

DEFAULT_INT = -9999
DEFAULT_FLOAT = np.nan
SN=0.2
dgamma = 2*0.01

# I reduced precision in some cases where
# it was not needed
# these all get copied in
GOLD_DT = [
    ('coadd_objects_id', 'i8'),
#    ('ra',               'f8'),
#    ('dec',              'f8'),
#    ('ebv',              'f4'),
#    ('flags_badregion',  'i4'),
#    ('flags_gold',       'i2'),
#    ('modest_class',     'i2'),
#    ('hpix',             'i8'),
#    ('n_exposure',       'i4'),
#    ('stamp_size',       'i2'),
    ('tilename',         'S12'), # this won't get copied
]

NAME_MAP=OrderedDict([
    ('flags',         {'name':'flags',      'dt':'i4', 'el':None}),
    ('mask_frac',     {'name':'mask_frac',  'dt':'f8', 'el':None}),

    ('psf_e1',        {'name':'psfrec_g',   'dt':'f8', 'el':0}),
    ('psf_e2',        {'name':'psfrec_g',   'dt':'f8', 'el':1}),
    ('psf_size',      {'name':'psfrec_T',   'dt':'f8', 'el':None}),

    ('mcal_psf_e1',   {'name':'mcal_gpsf',  'dt':'f8', 'el':0}),
    ('mcal_psf_e2',   {'name':'mcal_gpsf',  'dt':'f8', 'el':1}),
    ('mcal_psf_size', {'name':'mcal_Tpsf',  'dt':'f8', 'el':None}),
])

# for these we also have the sheared versions for each
# name
MCAL_NAME_MAP = OrderedDict([
    ('e1',         {'name':'mcal_g',     'dt':'f8', 'el':0}),
    ('e2',         {'name':'mcal_g',     'dt':'f8', 'el':1}),
    ('size',       {'name':'mcal_T_r',   'dt':'f8', 'el':None}),
    ('snr',        {'name':'mcal_s2n_r', 'dt':'f8', 'el':None}),
    ('flux_r',     {'name':'mcal_pars',  'dt':'f8', 'el':0}),  # el here is offset from 5
    ('flux_i',     {'name':'mcal_pars',  'dt':'f8', 'el':1}),  # el here is offset from 5
    ('flux_z',     {'name':'mcal_pars',  'dt':'f8', 'el':2}),  # el here is offset from 5
    ('covmat_0_0', {'name':'mcal_g_cov', 'dt':'f8', 'el':(0,0)}),
    ('covmat_0_1', {'name':'mcal_g_cov', 'dt':'f8', 'el':(0,1)}),
    ('covmat_1_1', {'name':'mcal_g_cov', 'dt':'f8', 'el':(1,1)}),

    # calculated
    ('weight',     {'name':'mcal_g_cov', 'dt':'f8', 'el':(0,1)}),
])

R_DT = [
    ('R11', 'f8'),
    ('R12', 'f8'),
    ('R21', 'f8'),
    ('R22', 'f8'),
]

MCAL_FIELDS = [
    'e1',
    'e2',
    'size',
    'snr',
    'weight',
    'covmat',
    'flux',
]

MCAL_TYPES= [
    'noshear',
    '1p',
    '1m',
    '2p',
    '2m',
]


class Namer(object):
    """
    create strings with a specified front prefix
    """
    def __init__(self, front=None, back=None):
        if front=='':
            front=None
        if back=='':
            back=None

        self.front=front
        self.back=back

        if self.front is None and self.back is None:
            self.nomod=True
        else:
            self.nomod=False

    def __call__(self, name):
        if self.nomod:
            return name
        else:
            n=name
            if self.front is not None:
                n = '%s_%s' % (self.front, n)
            if self.back is not None:
                n = '%s_%s' % (n, self.back)
            return n

def extract_tilename(fname):
    bname=os.path.basename(fname)
    return bname[0:12]

class Collator(object):
    """
    collate the tiles, renaming fields to a common standard.
    """
    def __init__(self, gold_fname, metacal_dir, output_file):
        self._gold_fname=gold_fname
        self._metacal_dir=metacal_dir
        self._load_flist()
        self._load_gold_cat()

        self._output_file=output_file

    
    def go(self):
        """
        match by tile and write the data
        """
        nfiles=len(self._flist)

        print("writing to:",self._output_file)

        first=True
        with fitsio.FITS(self._output_file,'rw',clobber=True) as fits:
            for i,fname in enumerate(self._flist):

                print("%d/%d %s" % (i+1,nfiles,fname))

                print("    reading")
                data = fitsio.read(fname)
                tilename=extract_tilename(fname)
                print("    tilename:",tilename)

                print("    matching",end='')
                tm0=time.time()
                wgold,=np.where(self._gold['tilename'] == tilename)

                ids = self._gold['coadd_objects_id'][wgold]

                mdata, mgold = eu.numpy_util.match(data['id'], ids)
                tm = time.time()-tm0

                print(" %d/%d matched %g sec" % (mdata.size,data.size,tm))

                #if mdata.size != data.size:
                #    raise RuntimeError("        %d/%d matched" % (mdata.size,data.size))

                print("    extracting output")
                data=data[mdata]
                gold = self._gold[wgold[mgold]]

                odata = self._extract_output(data, gold)

                if first:
                    first=False
                    fits.write(odata)
                else:
                    fits[-1].append(odata)

    def _extract_output(self, data, gold): 
        """
        extract the outputs, following the name mapping.

        Calculate any extra fields
        """
        output = self._get_collated_struct(data.size)

        eu.numpy_util.copy_fields(gold, output)

        for name,nm in NAME_MAP.iteritems():
            copy_data = self._get_copy_data(data, nm['name'], nm['el'])
            output[name] = copy_data

        for t_oname,nm in MCAL_NAME_MAP.iteritems():
            for type in MCAL_TYPES:
                namer = self._get_namer(type=type)

                oname = namer( t_oname )
                dname = namer( nm['name'] )

                if 'flux' in oname:
                    el = 5 + nm['el']
                    copy_data = data[dname][:,el]
                elif 'covmat' in oname:
                    el1,el2 = nm['el']
                    copy_data = data[dname][:,el1,el2]
                elif 'weight' in oname:
                    el1,el2 =nm['el']
                    copy_data = 1.0/(
                        2*SN**2 +
                        data[dname][:,el1,el1] +
                        data[dname][:,el2,el2]
                    )
                else:
                    copy_data = self._get_copy_data(data, dname, nm['el'])

                output[oname] = copy_data

        w,=np.where(output['flags']==0)
        if w.size > 0:
            output['R11'][w] = (output['e1_1p'][w] - output['e1_1m'][w])/dgamma
            output['R12'][w] = (output['e1_2p'][w] - output['e1_2m'][w])/dgamma
            output['R21'][w] = (output['e2_1p'][w] - output['e2_1m'][w])/dgamma
            output['R22'][w] = (output['e2_2p'][w] - output['e2_2m'][w])/dgamma

        return output

    def _get_copy_data(self, data, name, el):
        """
        deal with non-scalar data
        """
        if el is None:
            copy_data = data[name]
        elif isinstance(el,tuple):
            el1,el2 = el
            copy_data = data[name][:,el1,el2]
        else:
            copy_data = data[name][:,el]

        return copy_data
        
    def _load_gold_cat(self):
        print("reading:",self._gold_fname)
        cols=[c[0] for c in GOLD_DT]
        self._gold = fitsio.read(
            self._gold_fname,
            columns=cols,
            lower=True,
        )

    def _load_flist(self):
        """
        get the list of files
        """
        pattern = 'DES*blind.fits'
        pattern = os.path.join(self._metacal_dir, pattern)

        self._flist = glob.glob(pattern)
        self._flist.sort()

        if len(self._flist) == 0:
            raise RuntimeError("found no files '%s'" % pattern)

    def _get_collated_struct(self, num):
        dt = self.get_collated_dtype()

        st = np.zeros(num, dtype=dt)

        for i,dt in enumerate(st.dtype.descr):
            name=dt[0]
            dtype=dt[1]
            if 'i' in dtype:
                st[name] = DEFAULT_INT
            elif 'f' in dtype:
                st[name] = DEFAULT_FLOAT

        return st

    def get_collated_dtype(self):
        """
        build up the output data type from the
        name map dict, plus anything we are getting
        from gold or calculating
        """
        dt = []

        dt += [d for d in GOLD_DT if d[0] != 'tilename']

        for name,nm in NAME_MAP.iteritems():
            dt.append( (name, nm['dt']) )

        for name,nm in MCAL_NAME_MAP.iteritems():
            for type in MCAL_TYPES:
                namer = self._get_namer(type=type)

                mcal_name = namer(name)
                dt.append( (mcal_name, nm['dt']) )

        dt += R_DT
        return dt

    def _get_namer(self, type=None):
        if type == 'noshear':
            back=None
        else:
            back=type

        return Namer(back=back)



def main():

    args=parser.parse_args()

    collator = Collator(
        args.gold_file,
        args.metacal_dir,
        args.output_file,
    )

    collator.go()

if __name__=="__main__":
    main()
