-- inner, only the matches.  We will combine this with
-- the query against main later
select
    mof.coadd_objects_id,

    -- these will be combined later into simply mof_flags
    mof.flags               as mof_all_flags,
    mof.cm_mof_flags        as mof_iter_flags,

    mof.flux_mof_g          as mof_flux_g,
    mof.flux_mof_r          as mof_flux_r,
    mof.flux_mof_i          as mof_flux_i,
    mof.flux_mof_z          as mof_flux_z,
    mof.fluxerr_mof_g       as mof_fluxerr_g,
    mof.fluxerr_mof_r       as mof_fluxerr_r,
    mof.fluxerr_mof_i       as mof_fluxerr_i,
    mof.fluxerr_mof_z       as mof_fluxerr_z,

    coadd.tilename,
    coadd.flags_g,
    coadd.flags_r,
    coadd.flags_i,
    coadd.flags_z

from
    des_admin.y1a1_mof_d04 mof

    inner join des_admin.y1a1_gold gold
        on mof.coadd_objects_id = gold.coadd_objects_id

    inner join y1a1_coadd_objects_d04 coadd
        on gold.coadd_objects_id = coadd.coadd_objects_id

