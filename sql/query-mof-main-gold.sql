-- left join:  we want everything in gold.  Remember we
-- will be matching to metacal and im3shape so we need
-- everything

-- we are doing a separate inner join for the d04 mof catalog,
-- will copy in that information as needed

-- remember to run with --replace-none=-9999
select
    gold.coadd_objects_id,
    gold.ra,
    gold.dec,
    gold.ebv,
    gold.flags_gold,
    gold.modest_class,
    gold.hpix,

    coadd.tilename,
    coadd.flags_g,
    coadd.flags_r,
    coadd.flags_i,
    coadd.flags_z,

    -- these will be combined later into simply mof_flags
    mof.flags               as mof_all_flags,
    mof.cm_mof_flags        as mof_iter_flags,

    mof.flux_mof_g          as mof_flux_g,
    mof.flux_mof_r          as mof_flux_r,
    mof.flux_mof_i          as mof_flux_i,
    mof.flux_mof_z          as mof_flux_z,
    mof.fluxerr_mof_g       as mof_fluxerr_g,
    mof.fluxerr_mof_r       as mof_fluxerr_r,
    mof.fluxerr_mof_i       as mof_fluxerr_i,
    mof.fluxerr_mof_z       as mof_fluxerr_z
from
    des_admin.y1a1_gold gold

    -- the mof catalog for spt+s82
    left join des_admin.y1a1_mof mof
        on gold.coadd_objects_id = mof.coadd_objects_id

    -- grab some info from the coadd objects table
    left join y1a1_coadd_objects coadd
        on gold.coadd_objects_id = coadd.coadd_objects_id

order by
    gold.coadd_objects_id
