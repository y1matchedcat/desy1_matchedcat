import os
import distutils
from distutils.core import setup, Extension, Command
import glob

scripts = glob.glob('./bin/*')
scripts = [os.path.basename(f) for f in scripts if f[-1] != '~']
scripts = [os.path.join('bin',s) for s in scripts]

setup(
    name="desy1_matchedcat", 
    packages=['desy1_matchedcat'],
    scripts=scripts,
    version="0.9.0",
)




