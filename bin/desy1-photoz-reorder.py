"""
re-order the catalog to match line-by-line with the referece
gold catalog
"""
from __future__ import print_function
import numpy
from numpy import arange, zeros, where
import fitsio
import esutil as eu
import esutil.numpy_util as nu
import time
import argparse

DEFAULT = -9999
PDEFAULT = 9999

CHUNKSIZE = 1000000

class Orderer(object):
    """
    take the collated file and re-order it to match the order
    of the gold file

    For objects that do not match, gold data will be copied in
    but shear data will not

    Need to set new flags
    """
    def __init__(self,
                 gold_file,
                 shear_file,
                 output_file):

        self._gold_file=gold_file
        self._shear_file=shear_file
        self._output_file=output_file


        print("writing to:",self._output_file)

        assert self._gold_file != self._output_file
        assert self._shear_file != self._output_file

        # self._load_gold_ids()
        self._load_shear_ids()

        self._set_dtype()

    def _load_shear_ids(self):
        """
        load the shear ids for matching
        """
        print("reading ids from:",self._shear_file)
        self._shear_ids = fitsio.read(
            self._shear_file,
            columns='coadd_objects_id',
        )

    def go(self, test_nchunks=None):
        """
        write new shear and gold files that line up 1-1
        """

        chunksize = CHUNKSIZE
        first=True

        with fitsio.FITS(self._output_file,'rw',clobber=True) as fits:

            with fitsio.FITS(self._gold_file) as gfits:

                ngold = gfits[1].get_nrows()
                nchunks = self._get_chunk_info(ngold, test_nchunks=test_nchunks)

                for chunk in xrange(nchunks):

                    print("chunk %d/%d" % (chunk+1,nchunks))

                    start = chunk*chunksize
                    end = (chunk+1)*chunksize

                    gdata = self._read_from_gold(gfits, start, end)

                    odata = self._get_matched_data(gdata)

                    print("    writing ",end='')
                    tm0=time.time()
                    if first:
                        first=False
                        fits.write(odata)
                    else:
                        fits[-1].append(odata)
                    tm=time.time()-tm0
                    print("%g sec" % tm)

    def _get_matched_data(self, gdata):
        """
        get versions of the data that line up row-by-row
        """

        odata = self._make_combined_struct(gdata.size)

        nu.copy_fields(gdata, odata)

        mo, ms = self._match_gold(
            odata['coadd_objects_id'],
            self._shear_ids,
        )
        nmatch = mo.size
        if nmatch == 0:
            return odata

        sdata = self._read_shear_subset(ms)

        # fitsio will return the data in row-ascending order, so we need to
        # re-match

        mo, ms = self._match_gold(
            odata['coadd_objects_id'],
            sdata['coadd_objects_id'],
        )

        if mo.size != nmatch:
            raise ValueError("number of matches changed: "
                             "orig: %d new: %d" % (nmatch, mo.size))

        self._copy_from_shear(odata, sdata, mo, ms)

        return odata

    def _read_from_gold(self, fits, start, end):
        print("    reading from gold",end='')

        tm0=time.time()
        data = fits[1][start:end]

        print(" %g sec" % (time.time()-tm0))

        return data

    def _match_gold(self, gids, sids):
        print("    matching",end='')
        tm0=time.time()
        mg, ms = eu.numpy_util.match(gids, sids)
        tm=time.time()-tm0
        nmatch=mg.size
        print(mg.size)
        print(" %d/%d matched %g sec" % (mg.size,gids.size,tm))

        return mg, ms

    def _read_shear_subset(self, index):
        print("    reading the shear subset",end='')
        tm0=time.time()
        data = fitsio.read(self._shear_file, rows=index)
        tm=time.time()-tm0
        print(" %g sec" % tm)

        return data

    def _copy_from_shear(self, data, sdata, mo, ms):
        print("    copying shear data",end='')
        tm0 = time.time()
        for n in sdata.dtype.names:
            data[n][mo] = sdata[n][ms]
        print(" %g sec" % (time.time()-tm0))

    def _get_chunk_info(self, ngold, test_nchunks=None):
        chunksize = CHUNKSIZE

        nchunks = ngold // chunksize
        if (ngold % chunksize) != 0:
            nchunks += 1

        if test_nchunks is not None and test_nchunks < nchunks:
            nchunks = test_nchunks

        return nchunks

    def _make_combined_struct(self, n):
        """
        get the combined structure
        """
        st = zeros(n, dtype=self._dtype)
        set_defaults(st)

        return st

    def _set_dtype(self):
        """
        get a combined dtype, avoiding duplicates
        """
        tmp = fitsio.read(self._shear_file, rows=[0])

        dt = tmp.dtype.descr
        names = tmp.dtype.names

        self._dtype = dt


def set_defaults(st):
    for name in st.dtype.names:
        if 'weight' not in name:
            # we want to leave weight at zero for bad objects
            if 'cov' in name or 'err' in name:
                # default to large positive values for errors
                # and covariances
                st[name] = PDEFAULT
            else:
                st[name] = DEFAULT


parser=argparse.ArgumentParser()

parser.add_argument('gold_file',help='gold catalog with tilename')
parser.add_argument('shear_file',help='file with shear info')
parser.add_argument('output_file',help='output file to write')
#parser.add_argument('gold_output_file',help='matched gold output file to write')

parser.add_argument('--test-nchunks',type=int,
                    help='do a test with the specified number of chunks')

def main():

    args=parser.parse_args()

    orderer = Orderer(
        args.gold_file,
        args.shear_file,
        args.output_file,
    )

    orderer.go(test_nchunks=args.test_nchunks)

if __name__=="__main__":
    main()