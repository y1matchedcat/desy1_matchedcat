"""
re-order the catalog to match line-by-line with the referece
gold catalog
"""
from __future__ import print_function
import numpy
from numpy import arange, zeros, where
import fitsio
import esutil as eu
import esutil.numpy_util as nu
import time
import argparse

DEFAULT = -9999
PDEFAULT = 9999
SDEFAULT = 'None'
FLAG_NOT_MEASURED = 2**30
CHUNKSIZE = 1000000

MOF_BANDS=['g','r','i','z']
# BPZ cannot deal with negative fluxes
# this corresponds to a mag of 45
MINFLUX = 1.0e-6
# 0 : dec > -60 (gold file)
# 1 : modest_class (gold file)
# 2 : flags_badregion 0 (gold file)
# 3 : flags_gold 0 (gold file)
# 4 : flags_mof 0 (mof file)
# 5 : info_flag 0 (i3 file) (need to disable gold and modest flags in info flag in im3shape matched file)

IM3_SELECT_FLAGS = {
    'dec_cut': 2**0,
    'modest_class': 2**1,
    'flags_badregion': 2**2,
    'flags_gold': 2**3,
    'flags_mof': 2**4,
    'info_flag': 2**5,
    'mof_lowflux': 2**6,
    'no_nbc': 2**7,
    'is_edge': 2**8,
    'is_blist': 2**9,
    'not_measured': FLAG_NOT_MEASURED
}

IM3COLS = [('coadd_objects_id', 'i8'),
           ('e1', 'f8'),
           ('e2', 'f8'),
           ('m', 'f8'),
           ('c1', 'f8'),
           ('c2', 'f8'),
           ('weight', 'f8'),
           ('radius', 'f8'),
           ('snr', 'f8'),
           ('likelihood', 'f8'),
           ('chi2_pixel', 'f8'),
           ('flags', 'i4'),
           ('covmat_0_0', 'f8'),
           ('covmat_0_1', 'f8'),
           ('covmat_1_1', 'f8'),
           ('rgpp_rp', 'f8'),
           ('psf_e1', 'f8'),
           ('psf_e2', 'f8'),
           ('psf_size', 'f8'),
           ('bulge_fraction', 'f8'),
           ('nexp', 'i4'),
           ('stamp_size', 'i4'),
           ('mask_fraction', 'f8'),
           ('flags_select', 'i4')
           ]

GOLD_COLS_TO_COPY = ['ra', 'dec', 'tilename']

DEC_MIN = -60.

class Orderer(object):
    """
    take the collated file and re-order it to match the order
    of the gold file

    For objects that do not match, gold data will be copied in
    but shear data will not

    Need to set new flags
    """
    def __init__(self,
                 gold_file,
                 shear_file,
                 output_file,
                 bad_ids_file,
                 blist_ids_file):

        self._gold_file=gold_file
        self._shear_file=shear_file
        self._output_file=output_file

        with open(bad_ids_file) as ifile:
            self.bad_ids = numpy.array([int(x) for x in ifile])

        with open(blist_ids_file) as ifile:
            self.blist_ids = numpy.array([int(x.split()[1]) for x in ifile])

        print("writing to:",self._output_file)

        assert self._gold_file != self._output_file
        assert self._shear_file != self._output_file

        # self._load_gold_ids()
        self._load_shear_ids()

        self._set_dtype()

    def _load_shear_ids(self):
        """
        load the shear ids for matching
        """
        print("reading ids from:",self._shear_file)
        self._shear_ids = fitsio.read(
            self._shear_file,
            columns='coadd_objects_id',
        )

    def go(self, test_nchunks=None):
        """
        write new shear and gold files that line up 1-1
        """

        chunksize = CHUNKSIZE
        first=True

        with fitsio.FITS(self._output_file,'rw',clobber=True) as fits:

            with fitsio.FITS(self._gold_file) as gfits:

                ngold = gfits[1].get_nrows()
                nchunks = self._get_chunk_info(ngold, test_nchunks=test_nchunks)

                for chunk in xrange(nchunks):

                    print("chunk %d/%d" % (chunk+1,nchunks))

                    start = chunk*chunksize
                    end = (chunk+1)*chunksize

                    gdata = self._read_from_gold(gfits, start, end)

                    odata = self._get_matched_data(gdata)

                    print("    writing ",end='')
                    tm0=time.time()
                    if first:
                        first=False
                        fits.write(odata)
                    else:
                        fits[-1].append(odata)
                    tm=time.time()-tm0
                    print("%g sec" % tm)

    def _get_matched_data(self, gdata):
        """
        get versions of the data that line up row-by-row
        """

        odata = self._make_combined_struct(gdata.size)

        nu.copy_fields(gdata, odata)

        mo, ms = self._match_gold(
            odata['coadd_objects_id'],
            self._shear_ids,
        )

        nmatch = mo.size
        if nmatch == 0:
            # Note flag columns are by default already set to FLAG_NOT_MEASURED
            # so returning early leaves that in place
            return odata

        sdata = self._read_shear_subset(ms)

        # fitsio will return the data in row-ascending order, so we need to
        # re-match

        mo, ms = self._match_gold(
            odata['coadd_objects_id'],
            sdata['coadd_objects_id'],
        )

        if mo.size != nmatch:
            raise ValueError("number of matches changed: "
                             "orig: %d new: %d" % (nmatch, mo.size))

        self._copy_from_shear(odata, sdata, mo, ms)

        self._set_select_flags(gdata, odata, mo)

        return odata

    def _read_from_gold(self, fits, start, end):
        print("    reading from gold",end='')

        tm0=time.time()
        data = fits[1][start:end]

        print(" %g sec" % (time.time()-tm0))

        return data

    def _match_gold(self, gids, sids):
        print("    matching",end='')
        tm0=time.time()
        mg, ms = eu.numpy_util.match(gids, sids)
        tm=time.time()-tm0
        nmatch=mg.size
        print(" %d/%d matched %g sec" % (mg.size,gids.size,tm))

        return mg, ms

    def _read_shear_subset(self, index):
        print("    reading the shear subset",end='')
        tm0=time.time()
        data = fitsio.read(self._shear_file, rows=index)
        tm=time.time()-tm0
        print(" %g sec" % tm)

        return data

    def _copy_from_shear(self, data, sdata, mo, ms):
        print("    copying shear data")
        tm0 = time.time()

        # for n in sdata.dtype.names:
        #     data[n][mo] = sdata[n][ms]
        copycols = ['coadd_objects_id', 'e1', 'e2', 'm', 'c1', 'c2', 'weight',
                    'radius', 'snr', 'likelihood', 'chi2_pixel']
        for col in copycols:
            data[col][mo] = sdata[col][ms]
        print('    copying part 1 finished')
        data['flags'][mo] = sdata['info_flag'][ms]
        data['covmat_0_0'][mo] = sdata['covmat_2_2'][ms]
        data['covmat_0_1'][mo] = sdata['covmat_2_3'][ms]
        data['covmat_1_1'][mo] = sdata['covmat_3_3'][ms]
        data['rgpp_rp'][mo] = sdata['mean_rgpp_rp'][ms]
        data['psf_e1'][mo] = sdata['mean_hsm_psf_e1_sky'][ms]
        data['psf_e2'][mo] = sdata['mean_hsm_psf_e2_sky'][ms]
        data['psf_size'][mo] = sdata['mean_psf_fwhm'][ms]
        data['bulge_fraction'][mo] = (sdata['bulge_flux'][ms] != 0.).astype(int)
        print('    copying part 2 finished')

        data['nexp'][mo] = sdata['n_exposure'][ms]
        data['stamp_size'][mo] = sdata['stamp_size'][ms]
        data['mask_fraction'][mo] = sdata['mean_mask_fraction'][ms]
        print('    copying part 3 finished')


        print(" %g sec" % (time.time()-tm0))

    def _get_chunk_info(self, ngold, test_nchunks=None):
        chunksize = CHUNKSIZE

        nchunks = ngold // chunksize
        if (ngold % chunksize) != 0:
            nchunks += 1

        if test_nchunks is not None and test_nchunks < nchunks:
            nchunks = test_nchunks

        return nchunks

    def _make_combined_struct(self, n):
        """
        get the combined structure
        """
        st = zeros(n, dtype=self._dtype)
        set_defaults(st)

        return st

    def _set_dtype(self):
        """
        get a combined dtype, avoiding duplicates
        """

        dt = IM3COLS
        names = [ic[0] for ic in IM3COLS]

        gold_tmp = fitsio.read(self._gold_file, rows=[0])
        for d in gold_tmp.dtype.descr:
            n = d[0]
            if n in GOLD_COLS_TO_COPY and n not in names:
                print("copying",n,"from gold")
                dt.append( d )

        self._dtype = dt

    def _set_select_flags(self, gdata, data, index):
        """
        Note flag columns are by default already set to FLAG_NOT_MEASURED

        gdata and data line up 1-1

        index is things for which we had an entry
        in the shear catalog

        parameters
        ----------
        gdata: array
            Gold data
        data: array
            Output array
        index: array
            Index into output array
        """

        print("    setting select flags")
        tm0 = time.time()

        fdict = IM3_SELECT_FLAGS

        # for things with a match, we start assuming we have something
        data['flags_select'][index] = 0

        # dec cut
        w, = where(gdata['dec'][index] < DEC_MIN)
        if w.size > 0:
            print("        %6d/%6d with dec "
                  "less than %g" % (w.size, index.size, DEC_MIN))
            data['flags_select'][index[w]] |= fdict['dec_cut']

        # modest class cut
        w, = where(gdata['modest_class'][index] != 1)
        if w.size > 0:
            print("        %6d/%6d with modest_class " % (w.size, index.size))
            data['flags_select'][index[w]] |= fdict['modest_class']

        # bad regions
        w, = where(gdata['flags_badregion'][index] != 0)
        if w.size > 0:
            print("        %6d/%6d with badregion set" % (w.size, index.size))
            data['flags_select'][index[w]] |= fdict['flags_badregion']

        # gold flags
        w, = where(gdata['flags_gold'][index] != 0)
        if w.size > 0:
            print("        %6d/%6d with gold flags set" % (w.size, index.size))
            data['flags_select'][index[w]] |= fdict['flags_gold']

        # mof flags
        w,=where(gdata['mof_flags'][index] != 0)
        if w.size > 0:
            print("        %6d/%6d with mof_flags " % (w.size, index.size))
            data['flags_select'][index[w]] |= fdict['flags_mof']

        # info_flags
        w, = where((data['flags'][index] > 2**2) * (data['flags'][index] < 0))
        if w.size > 0:
            print("        %6d/%6d with info_flag set" % (w.size, index.size))
            data['flags_select'][index[w]] |= fdict['info_flag']

        # info_flags
        w, = where((data['weight'][index] == 0.))
        if w.size > 0:
            print("        %6d/%6d with no nbc" % (w.size, index.size))
            data['flags_select'][index[w]] |= fdict['no_nbc']

        is_bad = numpy.in1d(data['coadd_objects_id'][index], self.bad_ids, assume_unique=True)
        if is_bad.sum() > 0:
            print("        %6d/%6d with edge_flag" % (is_bad.sum(), index.size))
            data['flags_select'][index[is_bad]] |= fdict['is_edge']

        is_blist = numpy.in1d(data['coadd_objects_id'][index], self.blist_ids, assume_unique=True)
        if is_blist.sum() > 0:
            print("        %6d/%6d with blist" % (is_blist.sum(), index.size))
            data['flags_select'][index[is_blist]] |= fdict['is_blist']

            # print(data['flags_select'][index[is_bad][:100]])

        # BPZ cannot handle negative fluxes.  We hope this cut doesn't bias
        # us too much
        self._check_mof_fluxes(gdata, data, index)

        print("     %g sec" % (time.time() - tm0))


    def _check_mof_fluxes(self, gdata, data, index):
        """
        parameters
        ----------
        gdata: array
            Gold data
        data: array
            Output data
        index: array
            Indexes for rows we are working with
        """

        fdict = IM3_SELECT_FLAGS

        for band in MOF_BANDS:

            field = 'mof_flux_%s' % band
            w, = numpy.where(gdata[field][index] < MINFLUX)

            if w.size > 0:
                m = "        %6d/%6d with low %s < %g"
                print(m % (w.size, index.size, field, MINFLUX))
                data['flags_select'][index[w]] |= fdict['mof_lowflux']


def set_defaults(st):

    for dt in st.dtype.descr:
        name = dt[0]

        if 'cov' in name or 'err' in name:
            # default to large positive values for errors
            # and covariances
            st[name] = PDEFAULT

        elif 'S' in dt[1]:
            st[name] = SDEFAULT

        elif 'flag' in name.lower():
            st[name] = FLAG_NOT_MEASURED
        else:
            st[name] = DEFAULT



parser=argparse.ArgumentParser()

parser.add_argument('gold_file',help='gold catalog with tilename')
parser.add_argument('shear_file',help='file with shear info')
parser.add_argument('edge_ids_file',help='input file for bad coadd ids')
parser.add_argument('blist_ids_file',help='input file for blist exposures')
parser.add_argument('output_file',help='output file to write')


parser.add_argument('--test-nchunks',type=int,
                    help='do a test with the specified number of chunks')

def main():

    args=parser.parse_args()

    orderer = Orderer(
        args.gold_file,
        args.shear_file,
        args.output_file,
        args.edge_ids_file,
        args.blist_ids_file
    )

    orderer.go(test_nchunks=args.test_nchunks)

if __name__=="__main__":
    main()
