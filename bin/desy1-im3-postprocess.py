import argparse
import fitsio as fio
import os
import glob
import numpy as np
import subprocess as sp

COL_MIN = 30
COL_MAX = 2048 - 30
ROW_MIN = 30
ROW_MAX = 4096 - 30


def get_bad_edges(in_folder, out_file):
    """From Joe Zuntz"""
    filenames = glob.glob(in_folder + '*fits')
    bad = []
    for filename in filenames:
        f = fio.FITS(filename)
        ext = f[1]

        data = ext.read_columns(["coadd_objects_id", "orig_row", "orig_col"])

        w = np.where(
            (data['orig_row'] < ROW_MIN) |
            (data['orig_row'] > ROW_MAX) |
            (data['orig_col'] < COL_MIN) |
            (data['orig_col'] > COL_MAX)
        )

        bad_rows = data['coadd_objects_id'][w]
        bad_ids = np.unique(bad_rows)
        print "{} - {} bad objects".format(filename, len(bad_ids))
        bad.append(bad_ids)

    bad = np.concatenate(bad)
    np.savetxt(out_file, bad, fmt='%d')


def copy_cat(im3_in_cat, im3_out_cat):

    cmd = 'scp ' + im3_in_cat + ' ' + im3_out_cat
    print cmd
    sp.call(cmd.split(' '))


def update_flags(im3_out_cat, ids_file):

    f = fio.FITS(im3_out_cat, mode="rw")
    cat = f[1]
    data = cat.read_columns(["coadd_objects_id", "flags_select"])
    print "Loaded"

    ids = data['coadd_objects_id']
    flags = data['flags_select']

    bad_ids = np.array([int(x) for x in open(ids_file)])
    print "Loaded ids"

    is_bad = np.in1d(ids, bad_ids, assume_unique=True)
    print "in1d"

    flags[is_bad] |= 2 ** 8
    print "flagged"

    cat.write_column('flags_select', flags)
    print "done"



parser=argparse.ArgumentParser()
parser.add_argument('in_folder',help='folder with raw im3 files')
parser.add_argument('ids_file',help='output file for bad ids')

# parser.add_argument('im3_in_cat',help='im3shape input catalog')
# parser.add_argument('im3_out_cat',help='im3shape output catalog')


if __name__ == "__main__":
    args = parser.parse_args()
    in_folder = args.in_folder
    ids_file = args.ids_file
    # im3_in_cat = args.im3_in_cat
    # im3_out_cat = args.im3_out_cat
    #
    get_bad_edges(in_folder, ids_file)
