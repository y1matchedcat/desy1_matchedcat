"""
Reformats GOLD and im3shape v4 into the planned format
"""

import argparse
import fitsio as fio
import os
import glob
import numpy as np

BADVAL = -9999
PBADVAL = 9999
SBADVAL = 'BADVAL'


def imformat(rawim3, rowmax=None):
    im3cols = [('coadd_objects_id', 'i8'),
        ('e1', 'f8'),
        ('e2', 'f8'),
        ('m', 'f8'),
        ('c1', 'f8'),
        ('c2', 'f8'),
        ('weight', 'f8'),
        ('radius', 'f8'),
        ('snr', 'f8'),
        ('likelihood', 'f8'),
        ('chi2_pixel', 'f8'),
        ('flags', 'i4'),
        ('covmat_0_0', 'f8'),
        ('covmat_0_1', 'f8'),
        ('covmat_1_1', 'f8'),
        ('rgpp_rp', 'f8'),
        ('psf_e1', 'f8'),
        ('psf_e2', 'f8'),
        ('psf_size', 'f8'),
        ('bulge_fraction', 'f8'),
        ('nexp', 'i4'),
        ('stamp_size', 'i4'),
        ('mask_fraction', 'f8'),
        ('flags_select', 'i4')
    ]
    
    nrows = int(rawim3.get_nrows())
    if rowmax is None:
        rowmax = nrows
    imres = np.zeros(rowmax, dtype=im3cols)
    for i, col in enumerate(imres.dtype.names):
        if 'covmat' in col:
            imres[col] = PBADVAL
        elif 'S' in im3cols[i][1]:
            imres[col] = SBADVAL
        else:
            imres[col] = BADVAL
    print 'created table'

    # columns which can simply be copied
    copycols = ['coadd_objects_id', 'e1', 'e2', 'm', 'c1', 'c2', 'weight',
                'radius', 'snr', 'likelihood', 'chi2_pixel']
    for col in copycols:
        imres[col] = rawim3[col][:rowmax]
    print 'copying part 1 finished'

    imres['flags'] = rawim3['info_flag'][:rowmax]
    imres['covmat_0_0'] = rawim3['covmat_2_2'][:rowmax]
    imres['covmat_0_1'] = rawim3['covmat_2_3'][:rowmax]
    imres['covmat_1_1'] = rawim3['covmat_3_3'][:rowmax]
    imres['rgpp_rp'] = rawim3['mean_rgpp_rp'][:rowmax]
    imres['psf_e1'] = rawim3['mean_hsm_psf_e1_sky'][:rowmax]
    imres['psf_e2'] = rawim3['mean_hsm_psf_e2_sky'][:rowmax]
    imres['psf_size'] = rawim3['mean_psf_fwhm'][:rowmax]
    imres['bulge_fraction'] = (rawim3['bulge_flux'][:rowmax] != 0.).astype(int)
    print 'copying part 2 finished'

    imres['nexp'] = rawim3['n_exposure'][:rowmax]
    imres['stamp_size'] = rawim3['stamp_size'][:rowmax]
    imres['mask_fraction'] = rawim3['mean_mask_fraction'][:rowmax]
    print 'copying part 3 finished'

    return imres


parser=argparse.ArgumentParser()
parser.add_argument('shear_file',help='file with shear info')
parser.add_argument('output_file',help='output file to write')

ROWMAX = None
if __name__ == '__main__':

    args = parser.parse_args()
    im3name = args.shear_file
    im3_res_name = args.output_file
    
    rawim3 = fio.FITS(im3name, 'r')[1]

    print 'formatting im3shape'
    with fio.FITS(im3_res_name, 'rw', clobber=True) as imfits:
        imres = imformat(rawim3, rowmax=ROWMAX)
        imfits.write(imres)
