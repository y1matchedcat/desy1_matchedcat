from __future__ import print_function
import numpy
from numpy import arange, zeros, where
import fitsio
import esutil as eu
import esutil.numpy_util as nu
import time
import glob
import argparse

def collator(names, outname):
    print('opening ', outname)
    with fitsio.FITS(outname, 'rw', clobber=True) as outfits:
        first = True
        for name in names:
            print('reading', name)

            with fitsio.FITS(name, 'r') as infits:

                odata = infits[1][:]
                names = odata.dtype.names
                names_new = []
                for dt in names:
                    names_new.append(dt.lower())
                odata.dtype.names = names_new

                if first:
                    first = False
                    outfits.write(odata)
                else:
                    outfits[-1].append(odata)

parser=argparse.ArgumentParser()

parser.add_argument('input_expr', type=str, help='regexp pattern')
parser.add_argument('collate_file', type=str, help='matched gold output file to write')


if __name__=="__main__":
    args = parser.parse_args()
    # print(args.input_expr)
    # print(args)
    names = glob.glob(args.input_expr)
    print(names)
    collator(names, args.collate_file)





